package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/enbis/testex/helper"
)

type Response struct {
	Status string `json:"status"`
	Error  string `json:"error,omitempty"`
	Result string `json:"result,omitempty"`
}

func Handler(w http.ResponseWriter, r *http.Request) {
	var res Response

	queryValue := r.URL.Query().Get("value")

	converted, err := helper.Convert(queryValue)
	if err != nil {
		res = Response{Status: "fail", Error: err.Error()}
		sendResponse(w, res)
		return
	}

	incremented := helper.Increment(converted)
	res = Response{Status: "success", Result: helper.ToString(incremented)}
	sendResponse(w, res)
}

func sendResponse(w http.ResponseWriter, res Response) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(res)
}
