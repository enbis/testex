// +build integration

package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHandler(t *testing.T) {
	var response Response

	req := httptest.NewRequest("GET", "/?value=10", nil)
	w := httptest.NewRecorder()
	Handler(w, req)

	res := w.Result()
	defer res.Body.Close()

	assert.Equal(t, http.StatusOK, res.StatusCode)

	data, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)

	err = json.Unmarshal(data, &response)
	assert.NoError(t, err)

	assert.Equal(t, "11", response.Result)
}
