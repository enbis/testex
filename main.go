package main

import (
	"net/http"

	"log"

	"gitlab.com/enbis/testex/handler"
)

func main() {
	http.HandleFunc("/", handler.Handler)

	if err := http.ListenAndServe(":8099", nil); err != nil {
		log.Fatal(err.Error())
	}
}
