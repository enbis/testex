package helper

import (
	"strconv"
)

func Convert(v string) (int, error) {
	return strconv.Atoi(v)
}

func Increment(v int) int {
	return v + 1
}

func ToString(in int) string {
	return strconv.Itoa(in)
}
