// +build unit

package helper

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConvert(t *testing.T) {
	tables := []struct {
		in  string
		out int
	}{
		{"1", 1},
		{"100", 100},
	}

	for _, table := range tables {
		converted, err := Convert(table.in)
		assert.NoError(t, err)
		assert.Equal(t, table.out, converted)
	}
}

func TestIncrement(t *testing.T) {
	tables := []struct {
		in  int
		out int
	}{
		{1, 2},
		{100, 101},
	}

	for _, table := range tables {
		converted := Increment(table.in)
		assert.Equal(t, table.out, converted)
	}
}

func TestToString(t *testing.T) {
	tables := []struct {
		in  int
		out string
	}{
		{1, "1"},
		{100, "100"},
	}

	for _, table := range tables {
		str := ToString(table.in)
		assert.Equal(t, table.out, str)
	}
}
